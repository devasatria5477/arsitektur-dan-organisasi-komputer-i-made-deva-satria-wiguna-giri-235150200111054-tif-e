import java.util.*;

public class hmm {
    public static void main(String[] args) {
        Scanner in = new Scanner (System.in);

        int a;

        do {
            System.out.println("Pilih konversi bilangan:");
            System.out.println("1. Biner ke Desimal");
            System.out.println("2. Desimal ke Biner");
            System.out.println("3. Biner ke Heksadesimal");
            System.out.println("4. Heksadesimal ke Biner");
            System.out.println("5. Desimal ke Heksadesimal");
            System.out.println("6. Heksadesimal ke Desimal");
            System.out.println("0. Keluar");

            a = in.nextInt();

            switch (a) {
                case 1:
                    System.out.print("Masukkan bilangan biner: ");
                    String biner = in.next();
                    int desimal = Integer.parseInt(biner, 2);
                    System.out.println("Hasil konversi: " + desimal);
                    break;
                case 2:
                    System.out.print("Masukkan bilangan desimal: ");
                    int desimalInput = in.nextInt();
                    String binerOutput = Integer.toBinaryString(desimalInput);
                    System.out.println("Hasil konversi: " + binerOutput);
                    break;
                case 3:
                    System.out.print("Masukkan bilangan biner: ");
                    String binerInput = in.next();
                    int desimalOutput = Integer.parseInt(binerInput, 2);
                    String heksaOutput = Integer.toHexString(desimalOutput);
                    System.out.println("Hasil konversi: " + heksaOutput.toUpperCase());
                    break;
                case 4:
                    System.out.print("Masukkan bilangan heksadesimal: ");
                    String heksaInput = in.next();
                    int desimalFromHeks = Integer.parseInt(heksaInput, 16);
                    String binerFromHeks = Integer.toBinaryString(desimalFromHeks);
                    System.out.println("Hasil konversi: " + binerFromHeks);
                    break;
                case 5:
                    System.out.print("Masukkan bilangan desimal: ");
                    int desimalToHeks = in.nextInt();
                    String heksaOutput2 = Integer.toHexString(desimalToHeks);
                    System.out.println("Hasil konversi: " + heksaOutput2.toUpperCase());
                    break;
                case 6:
                    System.out.print("Masukkan bilangan heksadesimal: ");
                    String heksaInput2 = in.next();
                    int desimalFromHeks2 = Integer.parseInt(heksaInput2, 16);
                    System.out.println("Hasil konversi: " + desimalFromHeks2);
                    break;
                case 0:
                    System.out.println("Keluar dari program.");
                    break;
                default:
                    System.out.println("Pilihan tidak valid. Silakan coba lagi.");
            }
        } while (a != 0);

        in.close();

    }
}